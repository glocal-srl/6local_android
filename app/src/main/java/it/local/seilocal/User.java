package it.local.seilocal;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class User {

	public static boolean isLogged(Activity activity) {
		
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
		
		boolean defaultValue =  false;
		
		boolean logged = sharedPref.getBoolean("logged", defaultValue);
		
		return logged;
	}
	
	public static void logout(Activity activity) {
		//rimuove informazioni sul login
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);	
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putBoolean("logged",false);
		editor.commit();
			
	}

	public static void saveAssociato(Activity activity,String denominazione,String id) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
		
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putBoolean("logged",true);
		editor.putString("tipo","associato");
		editor.putString("id",id);
		editor.putString("denominazione",denominazione);
		editor.commit();
		
	}
	
	public static void saveTesserato(Activity activity,String id) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
		
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putBoolean("logged",true);
		editor.putString("tipo","tesserato");
		editor.putString("id",id);
		editor.commit();
		
	}
	
	public static void saveTessera(Activity activity,String numero) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
		
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("numero",numero);
		editor.commit();
		
	}
	
	public static void saveSpesa(Activity activity, float spesa) {
		// TODO Auto-generated method stub
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
		
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putFloat("spesa", spesa);
		editor.commit();
		
	}
	
	public static void savePunti(Activity activity, float punti) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
		
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putFloat("punti", punti);
		editor.commit();
		
	}
	
	public static void saveToken(Activity activity, String token) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
		
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("token", token);
		editor.commit();
		
	}
	
	public static String getTipo(Activity activity) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);	
		return sharedPref.getString("tipo", "");	
	}
	
	public static String getId(Activity activity) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);	
		return sharedPref.getString("id", "");
			
	}

	public static CharSequence getDenominazione(Activity activity) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);	
		return sharedPref.getString("denominazione", "");
	}
	
	public static CharSequence getTessera(Activity activity) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);	
		return sharedPref.getString("numero", "");
	}
	
	public static float getSpesa(Activity activity) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);	
		return sharedPref.getFloat("spesa", 0);
	}

	public static float getPunti(Activity activity) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);	
		return sharedPref.getFloat("punti", 0);
	}

	public static String getToken(Activity activity) {
		SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);	
		return sharedPref.getString("token", "");
	}

	

	

}
