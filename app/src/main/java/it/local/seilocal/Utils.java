package it.local.seilocal;

import java.util.Random;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationManager;

public class Utils {
	
	public static String randomString(int length) {
		String alphabet = 
		        new String("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"); 
		int n = alphabet.length(); 

		String result = new String(); 
		Random r = new Random(); 

		for (int i=0; i<length; i++) 
		    result = result + alphabet.charAt(r.nextInt(n));

		return result;
	  }
	
	public static String estraiNumero(String numero) {
		
		String[] parts = numero.split("/");
		return parts[parts.length - 1];
		
	  }

	public static boolean isNumber(String number) {
		try  
		   {  
			  Float.parseFloat(number);
		      return true;  
		   }  
		   catch(Exception e)  
		   {  
		      return false;  
		   }
		
	}

	public static CharSequence formattaNumeroTessera(CharSequence numeroTessera) {
		
		String nuovo = numeroTessera.subSequence(0, 3).toString();
		nuovo += " ";
		nuovo += numeroTessera.subSequence(3, 6).toString();
		nuovo += " ";
		nuovo += numeroTessera.subSequence(6, 9).toString();
		nuovo += " ";
		nuovo += numeroTessera.subSequence(9, 12).toString();
		
		return nuovo;
	}

	public static void problemaConnessione(Context context) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle("Attenzione");
		alertDialogBuilder
				.setMessage("Problemi di connessione")
				.setCancelable(false)
				.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		
	}

	public static Location getLastBestLocation(LocationManager mLocationManager) {
		Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		long GPSLocationTime = 0;
		if (null != locationGPS) { GPSLocationTime = locationGPS.getTime(); }

		long NetLocationTime = 0;

		if (null != locationNet) {
			NetLocationTime = locationNet.getTime();
		}

		if ( 0 < GPSLocationTime - NetLocationTime ) {
			return locationGPS;
		}
		else {
			return locationNet;
		}
	}


}
