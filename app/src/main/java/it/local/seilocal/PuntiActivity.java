package it.local.seilocal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class PuntiActivity extends Activity {
	
	final Context context = this;
	
	private float punti;
	private double puntiMeno;
	private float puntiPiu;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_punti);
		
		TextView tesseraTextView  = (TextView) findViewById(R.id.tessera);
		TextView puntiTextView = (TextView) findViewById(R.id.punti);
		
		
		tesseraTextView.setText(Utils.formattaNumeroTessera(User.getTessera(MainActivity.activity)));
		
		punti = User.getPunti(MainActivity.activity);
		puntiTextView.setText(Float.toString(punti));
		
		float spesa = User.getSpesa(MainActivity.activity);
		puntiPiu = spesa / 10;
		puntiPiu = (float)Math.floor(puntiPiu * 10)/10;
		
		puntiMeno = spesa / 20;
		//puntiMeno = Math.round(puntiMeno * 10)/10;
		
		//Log.i("MyActivity", "punti:" + punti);
		//Log.i("MyActivity", "puntiMeno:" + puntiMeno);
		//Log.i("MyActivity", "puntiPiu:" + puntiPiu);
		
		
		
		
		
		//DecimalFormat oneDigit = new DecimalFormat("#,##0.0");//format to 1 decimal place
	
		
		final Button btnPiu = (Button) findViewById(R.id.btn_piu);
		
		btnPiu.setText(String.valueOf(puntiPiu));
		
		final Button btnMeno = (Button) findViewById(R.id.btn_meno);
		final Button btnMeno10 = (Button) findViewById(R.id.btn_meno2);
		final Button btnMeno15 = (Button) findViewById(R.id.btn_meno3);
		final Button btnMeno20 = (Button) findViewById(R.id.btn_meno4);
		
	
		
		String puntiMenoStr = String.valueOf(Math.floor(puntiMeno * 10)/10);
		btnMeno.setText(puntiMenoStr);
		btnMeno10.setText(String.valueOf(Math.floor(puntiMeno * 2 * 10)/10));
		btnMeno15.setText(String.valueOf(Math.floor(puntiMeno * 3 * 10)/10));
		btnMeno20.setText(String.valueOf(Math.floor(puntiMeno * 4 * 10)/10));
		
		if(Math.floor(puntiMeno * 10)/10 > punti || puntiMeno < 0.1)
			btnMeno.setVisibility(View.INVISIBLE);
		
		if(Math.floor(puntiMeno * 10 * 2)/10 > punti || puntiMeno < 0.1)
			btnMeno10.setVisibility(View.INVISIBLE);
		
		if(Math.floor(puntiMeno * 10 * 3)/10 > punti || puntiMeno < 0.1)
			btnMeno15.setVisibility(View.INVISIBLE);
		
		if(Math.floor(puntiMeno * 10 * 4)/10 > punti || puntiMeno < 0.1)
			btnMeno20.setVisibility(View.INVISIBLE);
		
		//EditText denominazione = (EditText) findViewById(R.id.editText1);
		//denominazione.setText(User.getDenominazione(MainActivity.activity));
		
		btnPiu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				btnPiu.setEnabled(false);
				btnMeno.setEnabled(false);
				
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						//Log.i("MyActivity", "inizio trhead somma punti sulla card");
						// recupero i punti sulla tessera
						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(
								"http://app.6local.it/associato/operazione/");
						try {
							List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
					        nameValuePairs.add(new BasicNameValuePair("associato_id",User.getId(MainActivity.activity)));
					        nameValuePairs.add(new BasicNameValuePair("numeroTessera",User.getTessera(MainActivity.activity).toString()));
					        nameValuePairs.add(new BasicNameValuePair("spesa",String.valueOf(User.getSpesa(MainActivity.activity))));
					        nameValuePairs.add(new BasicNameValuePair("operazione","1"));
					        nameValuePairs.add(new BasicNameValuePair("punti",String.valueOf(puntiPiu)));
					        nameValuePairs.add(new BasicNameValuePair("token",User.getToken(MainActivity.activity)));
					        
					        

							post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

							HttpResponse response = client.execute(post);
							BufferedReader rd = new BufferedReader(
									new InputStreamReader(response.getEntity()
											.getContent()));

							//Log.i("MyActivity:", rd.readLine());
							String r = rd.readLine();
							//Log.i("MyActivity", "risp:" + r);
							
							if (r.equals("1")){
								//operazione conclusa
								Intent intent = new Intent(context, FineActivity.class);
				        		startActivity(intent);
							}
							
							

						} catch (Exception e) {
							runOnUiThread(new Runnable() {
				                @Override
				                public void run() {
				                		Utils.problemaConnessione(context);
				                }
					    	});
							btnPiu.setEnabled(true);
						}

					}
				});
				
				thread.start();
			}
		});
		
		btnMeno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				btnPiu.setEnabled(false);
				btnMeno.setEnabled(false);
				btnMeno10.setEnabled(false);
				btnMeno15.setEnabled(false);
				btnMeno20.setEnabled(false);
				
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						//Log.i("MyActivity", "inizio trhead somma punti sulla card");
						// recupero i punti sulla tessera
						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(
								"http://app.6local.it/associato/operazione/");
						try {
							List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
					        nameValuePairs.add(new BasicNameValuePair("associato_id",User.getId(MainActivity.activity)));
					        nameValuePairs.add(new BasicNameValuePair("numeroTessera",User.getTessera(MainActivity.activity).toString()));
					        nameValuePairs.add(new BasicNameValuePair("spesa",String.valueOf(User.getSpesa(MainActivity.activity))));
					        nameValuePairs.add(new BasicNameValuePair("operazione","0"));
					        nameValuePairs.add(new BasicNameValuePair("punti",String.valueOf(Math.floor(puntiMeno * 10)/10)));
					        nameValuePairs.add(new BasicNameValuePair("token",User.getToken(MainActivity.activity)));
					        
					        

							post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

							HttpResponse response = client.execute(post);
							BufferedReader rd = new BufferedReader(
									new InputStreamReader(response.getEntity()
											.getContent()));

							//Log.i("MyActivity:", rd.readLine());
							String r = rd.readLine();
							//Log.i("MyActivity", "risp:" + r);
							
							if (r.equals("1")){
								//operazione conclusa
								Intent intent = new Intent(context, FineActivity.class);
				        		startActivity(intent);
							}
							
							

						} catch (Exception e) {
							runOnUiThread(new Runnable() {
				                @Override
				                public void run() {
				                		Utils.problemaConnessione(context);
				                }
					    	});
							btnPiu.setEnabled(true);
							btnMeno.setEnabled(true);
							btnMeno10.setEnabled(true);
							btnMeno15.setEnabled(true);
							btnMeno20.setEnabled(true);
						}

					}
				});
				
				thread.start();
			}
		});
		
		btnMeno10.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				btnPiu.setEnabled(false);
				btnMeno.setEnabled(false);
				btnMeno10.setEnabled(false);
				btnMeno15.setEnabled(false);
				btnMeno20.setEnabled(false);
				
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						//Log.i("MyActivity", "inizio trhead somma punti sulla card");
						// recupero i punti sulla tessera
						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(
								"http://app.6local.it/associato/operazione/");
						try {
							List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
					        nameValuePairs.add(new BasicNameValuePair("associato_id",User.getId(MainActivity.activity)));
					        nameValuePairs.add(new BasicNameValuePair("numeroTessera",User.getTessera(MainActivity.activity).toString()));
					        nameValuePairs.add(new BasicNameValuePair("spesa",String.valueOf(User.getSpesa(MainActivity.activity))));
					        nameValuePairs.add(new BasicNameValuePair("operazione","0"));
					        nameValuePairs.add(new BasicNameValuePair("punti",String.valueOf(Math.floor(puntiMeno * 10 * 2)/10)));
					        nameValuePairs.add(new BasicNameValuePair("token",User.getToken(MainActivity.activity)));
					        
					        

							post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

							HttpResponse response = client.execute(post);
							BufferedReader rd = new BufferedReader(
									new InputStreamReader(response.getEntity()
											.getContent()));

							//Log.i("MyActivity:", rd.readLine());
							String r = rd.readLine();
							//Log.i("MyActivity", "risp:" + r);
							
							if (r.equals("1")){
								//operazione conclusa
								Intent intent = new Intent(context, FineActivity.class);
				        		startActivity(intent);
							}
							
							

						} catch (Exception e) {
							runOnUiThread(new Runnable() {
				                @Override
				                public void run() {
				                		Utils.problemaConnessione(context);
				                }
					    	});
							btnPiu.setEnabled(true);
							btnMeno.setEnabled(true);
							btnMeno10.setEnabled(true);
							btnMeno15.setEnabled(true);
							btnMeno20.setEnabled(true);
						}

					}
				});
				
				thread.start();
			}
		});
		
		btnMeno15.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				btnPiu.setEnabled(false);
				btnMeno.setEnabled(false);
				btnMeno10.setEnabled(false);
				btnMeno15.setEnabled(false);
				btnMeno20.setEnabled(false);
				
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						//Log.i("MyActivity", "inizio trhead somma punti sulla card");
						// recupero i punti sulla tessera
						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(
								"http://app.6local.it/associato/operazione/");
						try {
							List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
					        nameValuePairs.add(new BasicNameValuePair("associato_id",User.getId(MainActivity.activity)));
					        nameValuePairs.add(new BasicNameValuePair("numeroTessera",User.getTessera(MainActivity.activity).toString()));
					        nameValuePairs.add(new BasicNameValuePair("spesa",String.valueOf(User.getSpesa(MainActivity.activity))));
					        nameValuePairs.add(new BasicNameValuePair("operazione","0"));
					        nameValuePairs.add(new BasicNameValuePair("punti",String.valueOf(Math.floor(puntiMeno * 10 * 3)/10)));
					        nameValuePairs.add(new BasicNameValuePair("token",User.getToken(MainActivity.activity)));
					        
					        

							post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

							HttpResponse response = client.execute(post);
							BufferedReader rd = new BufferedReader(
									new InputStreamReader(response.getEntity()
											.getContent()));

							//Log.i("MyActivity:", rd.readLine());
							String r = rd.readLine();
							//Log.i("MyActivity", "risp:" + r);
							
							if (r.equals("1")){
								//operazione conclusa
								Intent intent = new Intent(context, FineActivity.class);
				        		startActivity(intent);
							}
							
							

						} catch (Exception e) {
							runOnUiThread(new Runnable() {
				                @Override
				                public void run() {
				                		Utils.problemaConnessione(context);
				                }
					    	});
							btnPiu.setEnabled(true);
							btnMeno.setEnabled(true);
							btnMeno10.setEnabled(true);
							btnMeno15.setEnabled(true);
							btnMeno20.setEnabled(true);
						}

					}
				});
				
				thread.start();
			}
		});
		
		btnMeno20.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				btnPiu.setEnabled(false);
				btnMeno.setEnabled(false);
				btnMeno10.setEnabled(false);
				btnMeno15.setEnabled(false);
				btnMeno20.setEnabled(false);
				
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						//Log.i("MyActivity", "inizio trhead somma punti sulla card");
						// recupero i punti sulla tessera
						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(
								"http://app.6local.it/associato/operazione/");
						try {
							List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
					        nameValuePairs.add(new BasicNameValuePair("associato_id",User.getId(MainActivity.activity)));
					        nameValuePairs.add(new BasicNameValuePair("numeroTessera",User.getTessera(MainActivity.activity).toString()));
					        nameValuePairs.add(new BasicNameValuePair("spesa",String.valueOf(User.getSpesa(MainActivity.activity))));
					        nameValuePairs.add(new BasicNameValuePair("operazione","0"));
					        nameValuePairs.add(new BasicNameValuePair("punti",String.valueOf(Math.floor(puntiMeno * 10 * 4)/10)));
					        nameValuePairs.add(new BasicNameValuePair("token",User.getToken(MainActivity.activity)));
					        
					        

							post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

							HttpResponse response = client.execute(post);
							BufferedReader rd = new BufferedReader(
									new InputStreamReader(response.getEntity()
											.getContent()));

							//Log.i("MyActivity:", rd.readLine());
							String r = rd.readLine();
							//Log.i("MyActivity", "risp:" + r);
							
							if (r.equals("1")){
								//operazione conclusa
								Intent intent = new Intent(context, FineActivity.class);
				        		startActivity(intent);
							}
							
							

						} catch (Exception e) {
							runOnUiThread(new Runnable() {
				                @Override
				                public void run() {
				                		Utils.problemaConnessione(context);
				                }
					    	});
							btnPiu.setEnabled(true);
							btnMeno.setEnabled(true);
							btnMeno10.setEnabled(true);
							btnMeno15.setEnabled(true);
							btnMeno20.setEnabled(true);
						}

					}
				});
				
				thread.start();
			}
		});
		
		
	}
}
