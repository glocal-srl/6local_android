package it.local.seilocal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;


public class TesseratoActivity extends Activity {
	
	final Context context = this;
	private WebView mWebView;  
	private String url = "http://app.6local.it";  
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tesserato);
		
		url = url + "/tessera/visualizza/" + User.getId(MainActivity.activity); 
		
		//carichiamo la webview dentro il layout specificato  
        mWebView = (WebView) findViewById(R.id.webview);  
        //mWebView.getSettings().setJavaScriptEnabled(true);  
        mWebView.getSettings().setLoadsImagesAutomatically(true);         
        //carichiamo finalmete la url  
        mWebView.setWebViewClient(new WebViewClient());  
        mWebView.loadUrl(url);
        
        Button logoutButton = (Button) findViewById(R.id.btn_logout_tesserato);
        
        logoutButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				User.logout(MainActivity.activity);
				Intent intent = new Intent(context, MainActivity.class);
        		startActivity(intent);
			}
		});
		
	}
}
