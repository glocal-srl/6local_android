package it.local.seilocal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class DettaglioActivity extends FragmentActivity {

    private GoogleMap mMap;
    private Double latitudine = 45.6672;
    private Double longitudine = 9.95887;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dettaglio);


        Bundle extras = getIntent().getExtras();

        TextView denominazione = (TextView) findViewById(R.id.denominazione);
        denominazione.setText(extras.getString("denominazione"));

        TextView categoria = (TextView) findViewById(R.id.categoria);
        categoria.setText(extras.getString("categoria"));

        TextView comune = (TextView) findViewById(R.id.comune);
        comune.setText(extras.getString("comune"));

        TextView indirizzo = (TextView) findViewById(R.id.indirizzo);
        indirizzo.setText(extras.getString("indirizzo"));


        TextView telefono = (TextView) findViewById(R.id.telefono);
        telefono.setText(extras.getString("telefono"));


        TextView email = (TextView) findViewById(R.id.email);
        email.setText(extras.getString("email"));


        TextView web = (TextView) findViewById(R.id.web);
        web.setText(extras.getString("web"));

        Button percorso = (Button) findViewById(R.id.percorso);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final Location l = Utils.getLastBestLocation(locationManager);

        percorso.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.google.it/maps/dir/"+ l.getLatitude() + ","+l.getLongitude()+"/" + latitudine.toString() + "," + longitudine.toString());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });



        latitudine = Double.valueOf(extras.getString("latitudine"));
        longitudine = Double.valueOf(extras.getString("longitudine"));


        setUpMapIfNeeded();


    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMap() {

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudine,longitudine),10));
        mMap.addMarker(new MarkerOptions().position(new LatLng(latitudine, longitudine)).title("Marker"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dettaglio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
