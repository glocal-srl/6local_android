package it.local.seilocal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class HomeActivity extends Activity {
	
	final Context context = this;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		
		Button leggi = (Button) findViewById(R.id.attiva_camera);
		TextView denominazione = (TextView) findViewById(R.id.denominazione);
		
		denominazione.setText(User.getDenominazione(MainActivity.activity));
		
		leggi.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//eseguo activity per lettura QR CODE
				Intent intent = new Intent(context, LeggiActivity.class);
        		startActivity(intent);
			}
		});
		
		
		Button logoutButton = (Button) findViewById(R.id.button2);
        
        logoutButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				User.logout(MainActivity.activity);
				Intent intent = new Intent(context, MainActivity.class);
        		startActivity(intent);
			}
		});
		
	}
}
