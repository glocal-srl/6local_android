package it.local.seilocal;



import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class MainActivity extends Activity {

	final Context context = this;
	public String username,password;
	public static Activity activity;
	
	
	private boolean isPackageInstalled(String packagename, Context context) {
	    PackageManager pm = context.getPackageManager();
	    try {
	        pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
	        return true;
	    } catch (NameNotFoundException e) {
	        return false;
	    }
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		activity = this;
		
		boolean installed = isPackageInstalled("com.google.zxing.client.android", context);
		
		if(!installed){
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					context);

			// set title
			alertDialogBuilder.setTitle("Attenzione");
			alertDialogBuilder
					.setMessage("Devi installare l'app per lettura QR CODE")
					.setCancelable(false)
					.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialog, int id) {
									// if this button is clicked, close
									// current activity
									Intent intent = new Intent(Intent.ACTION_VIEW); 
									intent.setData(Uri.parse("market://search?q=pname:com.google.zxing.client.android")); 
									startActivity(intent);
									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
			
			
		}
		
		
		//vediamo se l'utente è loggato o meno
		
		boolean logged = User.isLogged(MainActivity.this);
		String tipo = User.getTipo(MainActivity.this);
		//Log.i("MyActivity", "utente loggato:" + logged);
		//Log.i("MyActivity", "utente tipo:" + tipo);
		
		if(logged){
			if(tipo.equals("associato")){
				//apro activity per associato
				Intent intent = new Intent(context, HomeActivity.class);
        		startActivity(intent);
        	}
        	else if(tipo.equals("tesserato")){
        		//apro activity per tesserato
        		Intent intent = new Intent(context, TesseratoActivity.class);
        		startActivity(intent);
        		
        	}
			return;
		}
		
		Button loginButton = (Button) findViewById(R.id.btn_login);
		
		
		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				EditText text = (EditText) findViewById(R.id.editText1);
				username = text.getText().toString().trim();
				
				EditText text2 = (EditText) findViewById(R.id.editText2);
				password = text2.getText().toString().trim();

				//Log.i("MyActivity", "username:" + username);
				//Log.i("MyActivity", "password:" + password);
				
				if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							context);

					// set title
					alertDialogBuilder.setTitle("Attenzione");
					alertDialogBuilder
							.setMessage("Devi inserire username e password")
							.setCancelable(false)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// if this button is clicked, close
											// current activity
											dialog.cancel();
										}
									});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();

				}else{
					
					//dati inseriti, verifico se sono validi 
					Thread thread = new Thread(new Runnable(){
						@Override
						public void run() {
							HttpClient client = new DefaultHttpClient();
						    HttpPost post = new HttpPost("http://app.6local.it/associato/login");
						    try {
						    	
						    	String token = Utils.randomString(30);
						    	User.saveToken(MainActivity.activity, token);
						    	
						    	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
						        nameValuePairs.add(new BasicNameValuePair("username",username));
						        nameValuePairs.add(new BasicNameValuePair("password",password));
						        nameValuePairs.add(new BasicNameValuePair("token",token));
						        
						        post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
						   
						        HttpResponse response = client.execute(post);
						        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
						        
						        String json;
						        
						        json = rd.readLine();
						     
						        if (json.equals("0")){
						        	//Log.i("MyActivity", "username o password errate");
						        	runOnUiThread(new Runnable() {
						                @Override
						                public void run() {
						                	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
													context);

											// set title
											alertDialogBuilder.setTitle("Attenzione");
											alertDialogBuilder
													.setMessage("Username o password errate")
													.setCancelable(false)
													.setPositiveButton("Ok",
															new DialogInterface.OnClickListener() {
																public void onClick(
																		DialogInterface dialog, int id) {
																	// if this button is clicked, close
																	// current activity
																	dialog.cancel();
																}
															});

											// create alert dialog
											AlertDialog alertDialog = alertDialogBuilder.create();

											// show it
											alertDialog.show();
						                }
						            });
						        }
						        else{
						        	JSONObject jsonObject = new JSONObject(json);
						        	
						        	String tipo = jsonObject.getString("tipo");
						        	//Log.i("MyActivity", "tipo:" + tipo);
						        	
						        	//utente loggato, salvo nel dispositivo i dati
						        	
						        	
						        	if(tipo.equals("associato")){
						        		String denominazione = jsonObject.getString("denominazione");
						        		String id = jsonObject.getString("id");
						        		User.saveAssociato(MainActivity.this,denominazione,id);
						        		Intent intent = new Intent(context, HomeActivity.class);
						        		startActivity(intent);
						        	}
						        	else if(tipo.equals("tesserato")){
						        		//apro activity per tesserato
						        		String id = jsonObject.getString("id");
						        		User.saveTesserato(MainActivity.this,id);
						        		Intent intent = new Intent(context, TesseratoActivity.class);
						        		startActivity(intent);
						        	}
						        	
						        	
								}
						        
						        
						    } catch (Exception e) {
						    	runOnUiThread(new Runnable() {
					                @Override
					                public void run() {
					                		Utils.problemaConnessione(context);
					                }
						    	});
						    }
						}
						});
						thread.start();
				       
				}
			}
		});
		
	}

}
