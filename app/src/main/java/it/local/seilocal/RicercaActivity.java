package it.local.seilocal;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class RicercaActivity extends ActionBarActivity {

    final Context context = this;
    EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ricerca);

        ImageView search = (ImageView) findViewById(R.id.imageView);
        this.text = (EditText) findViewById(R.id.editText);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(context, Mappa2Activity.class);
                intent1.putExtra("url", text.getText().toString());

                Log.d("debug", text.getText().toString());
                startActivity(intent1);
            }
        });

    }

}
