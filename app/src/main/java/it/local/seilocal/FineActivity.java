package it.local.seilocal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FineActivity extends Activity {
	final Context context = this;
	
	public TextView puntiTextView;
	public String punti;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fine);
		
		TextView tesseraTextView  = (TextView) findViewById(R.id.tessera);
		tesseraTextView.setText(Utils.formattaNumeroTessera(User.getTessera(MainActivity.activity)));
		
		puntiTextView  = (TextView) findViewById(R.id.punti);
		
		Button leggi = (Button) findViewById(R.id.leggi_altro);
		
		leggi.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//eseguo activity per lettura QR CODE
				Intent intent = new Intent(context, LeggiActivity.class);
        		startActivity(intent);
			}
		});
		
		
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				Log.i("MyActivity", "inizio trhead lettura punti sulla card");
				// recupero i punti sulla tessera
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(
						"http://app.6local.it/tessera/punti/" + User.getTessera(MainActivity.activity));
				try {
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							1);

					post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					HttpResponse response = client.execute(post);
					BufferedReader rd = new BufferedReader(
							new InputStreamReader(response.getEntity()
									.getContent()));

					//Log.i("MyActivity:", rd.readLine());
					punti = rd.readLine();
					
					runOnUiThread(new Runnable() {
					     @Override
					     public void run() {
					    	 puntiTextView.setText(punti);	
					    }
					});

				} catch (Exception e) {
					runOnUiThread(new Runnable() {
		                @Override
		                public void run() {
		                		Utils.problemaConnessione(context);
		                }
			    	});
				}

			}
		});
		
		thread.start();

	}
}
