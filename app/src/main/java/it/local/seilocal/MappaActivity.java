package it.local.seilocal;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MappaActivity extends Activity {

    public String urlok = "http://app.6local.it/json/near/";
    public String url;

    private Context context;

    private static final String TAG_DENOMINAZIO = "denominazione";
    private static final String TAG_CATEGORIA= "categoria";
    private static final String TAG_DISTANZA = "distance";


    ArrayList<HashMap<String, String>> jsonlist = new ArrayList<HashMap<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        //this.urlok += extras.getString("url") + "/";

        //this.url = extras.getString("url");

        setContentView(R.layout.activity_mappa);
        new CreaLista(MappaActivity.this).execute();
    }

    private class CreaLista extends AsyncTask<String, Void, Void> {

        private ProgressDialog dialog;
        private MappaActivity mappaActivity;

        public CreaLista(MappaActivity mappaActivity) {
            this.mappaActivity = mappaActivity;

            context = mappaActivity;

            dialog = new ProgressDialog(context);
        }

        @Override
        protected void onPostExecute(final Void success) {

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            ListAdapter adapter = new SimpleAdapter(context, jsonlist, R.layout.contenuto_lista,
                    new String[]{TAG_DENOMINAZIO, TAG_CATEGORIA, TAG_DISTANZA},
                    new int[]{R.id.labelDenominazione, R.id.labelCategoria, R.id.labelDistanza});


            ListView list = (ListView) findViewById(R.id.listView);
            list.setAdapter(adapter);

            //// DetailView

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context, DettaglioActivity.class);

                intent.putExtra("denominazione", jsonlist.get(position).get("denominazione"));
                intent.putExtra("categoria", jsonlist.get(position).get("categoria"));
                intent.putExtra("comune", jsonlist.get(position).get("comune"));
                intent.putExtra("indirizzo", jsonlist.get(position).get("indirizzo"));
                intent.putExtra("telefono", jsonlist.get(position).get("telefono"));
                intent.putExtra("email", jsonlist.get(position).get("email"));
                intent.putExtra("web", jsonlist.get(position).get("web"));
                intent.putExtra("latitudine", jsonlist.get(position).get("latitudine"));
                intent.putExtra("longitudine", jsonlist.get(position).get("longitudine"));

                startActivity(intent);

            }
        });  ///Fine DetailLink

        }

        @Override
        protected Void doInBackground(String... params) {

            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Location l = Utils.getLastBestLocation(locationManager);


            JSONParser jParser = new JSONParser(); // get JSON data from URL

            float lat = (float) l.getLatitude();
            float lon = (float) l.getLongitude();

            String par;
            /*
            String str;

            if(url=="near") {
                urlok += lat + "/" + lon;
            }

            if(url=="search") {
                str = urlok + "a";
                Log.i("debug",urlok);
            }
            */

            urlok += lat + "/" + lon;

            par = jParser.makeServiceCall(urlok, JSONParser.GET);
            try {
                JSONArray json = new JSONArray(par);


                for (int i = 0; i < json.length(); i++) {

                    JSONObject b = json.getJSONObject(i);
                    JSONObject c = b.getJSONObject("Associato");
                    JSONObject d = b.getJSONObject("0");

                    String denominazione = c.getString(TAG_DENOMINAZIO);
                    String categoria = c.getString(TAG_CATEGORIA);

                    String distanza = d.getString(TAG_DISTANZA);


                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put(TAG_DENOMINAZIO, denominazione);
                    map.put(TAG_CATEGORIA, categoria);
                    map.put(TAG_DISTANZA, distanza + " km");
                    map.put("comune",c.getString("comune") + " (" + c.getString("provincia") + ")");
                    map.put("indirizzo",c.getString("indirizzo") + " " + c.getString("civico"));
                    map.put("telefono", c.getString("tel"));
                    map.put("latitudine", c.getString("latitudine"));
                    map.put("longitudine", c.getString("longitudine"));
                    map.put("web", c.getString("web"));
                    map.put("email", c.getString("email"));



                    jsonlist.add(map);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

    }
}
