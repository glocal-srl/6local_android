package it.local.seilocal;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class InizioActivity extends ActionBarActivity {


    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inizio);

        ImageView img1 = (ImageView) findViewById(R.id.imageButton);
        ImageView img2 = (ImageView) findViewById(R.id.imageButton2);
        ImageView img3 = (ImageView) findViewById(R.id.imageButton3);
        ImageView img4= (ImageView) findViewById(R.id.imageButton4);

        /// Link Affiliato
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //eseguo activity per lettura QR CODE
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            }
        });
        ///Link Tesserato
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(context, MainActivity.class);
                startActivity(intent1);
            }
        });

        //mappa
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(context, MappaActivity.class);
                intent1.putExtra("url", "near");
                startActivity(intent1);

            }
        });

        //cerca
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(context, RicercaActivity.class);
                startActivity(intent1);
            }
        });

    }



}
