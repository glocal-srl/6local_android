package it.local.seilocal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TesseraActivity extends Activity {
	
	final Context context = this;
	
	public CharSequence numeroTessera;
	public TextView puntiTextView;
	public CharSequence punti;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tessera);

		numeroTessera = User.getTessera(MainActivity.activity);
		
		TextView tessera = (TextView) findViewById(R.id.tessera);
		puntiTextView = (TextView) findViewById(R.id.punti);

		tessera.setText(Utils.formattaNumeroTessera(numeroTessera));
		
		Button conferma = (Button) findViewById(R.id.invio);
		conferma.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				EditText spesaEditText = (EditText) findViewById(R.id.spesa);
				String spesa = spesaEditText.getText().toString().trim();
				
				if (TextUtils.isEmpty(spesa)) {

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							context);

					// set title
					alertDialogBuilder.setTitle("Attenzione");
					alertDialogBuilder
							.setMessage("Devi inserire il valore della spesa")
							.setCancelable(false)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// if this button is clicked, close
											// current activity
											dialog.cancel();
										}
									});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();

				}
				else if (!Utils.isNumber(spesa)) {

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							context);

					// set title
					alertDialogBuilder.setTitle("Attenzione");
					alertDialogBuilder
							.setMessage("Il valore della spesa deve essere un numero")
							.setCancelable(false)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// if this button is clicked, close
											// current activity
											dialog.cancel();
										}
									});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();

				}
				else if ((Float.parseFloat(spesa) < 1) || (Float.parseFloat(spesa) > 20000)) {

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							context);

					// set title
					alertDialogBuilder.setTitle("Attenzione");
					alertDialogBuilder
							.setMessage("Valore spesa non valido")
							.setCancelable(false)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// if this button is clicked, close
											// current activity
											dialog.cancel();
										}
									});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();

				}
					
				else{
					User.saveSpesa(MainActivity.activity, Float.parseFloat(spesa));
					User.savePunti(MainActivity.activity, Float.parseFloat(punti.toString()));
					Intent intent = new Intent(context, PuntiActivity.class);
	        		startActivity(intent);
				}
			}
		});

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				//Log.i("MyActivity", "inizio trhead lettura punti sulla card");
				// recupero i punti sulla tessera
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(
						"http://app.6local.it/tessera/punti/" + numeroTessera);
				try {
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							1);

					post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					HttpResponse response = client.execute(post);
					BufferedReader rd = new BufferedReader(
							new InputStreamReader(response.getEntity()
									.getContent()));

					//Log.i("MyActivity:", rd.readLine());
					punti = rd.readLine();
					
					//controllo tessera non registrata
					if (punti.equals("NO REG")){
						runOnUiThread(new Runnable() {
						     @Override
						     public void run() {
						    	 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
											context);

									// set title
									alertDialogBuilder.setTitle("ATTIVAZIONE");
									alertDialogBuilder
											.setMessage("Attiva 6local card dal sito \nwww.6local.it")
											.setCancelable(false)
											.setPositiveButton("Ok",
													new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog, int id) {
															// if this button is clicked, close
															// current activity
															dialog.cancel();
															Intent intent = new Intent(context, HomeActivity.class);
											        		startActivity(intent);
														}
													});

									// create alert dialog
									AlertDialog alertDialog = alertDialogBuilder.create();

									// show it
									alertDialog.show();

						    }
						});
					}
					else
						if (punti.equals("NO ACT")){
							runOnUiThread(new Runnable() {
							     @Override
							     public void run() {
							    	 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
												context);

										// set title
										alertDialogBuilder.setTitle("CONFERMA ATTIVAZIONE");
										alertDialogBuilder
												.setMessage("Per completare la procedura di attivazione card, rispondi alla mail che ti abbiamo inviato")
												.setCancelable(false)
												.setPositiveButton("Ok",
														new DialogInterface.OnClickListener() {
															public void onClick(
																	DialogInterface dialog, int id) {
																// if this button is clicked, close
																// current activity
																dialog.cancel();
																Intent intent = new Intent(context, HomeActivity.class);
												        		startActivity(intent);
															}
														});

										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder.create();

										// show it
										alertDialog.show();

							    }
							});
						}
						else	
					runOnUiThread(new Runnable() {
					     @Override
					     public void run() {
					    	 puntiTextView.setText(punti);	
					    	 
					    	 Button conferma = (Button) findViewById(R.id.invio);
					    	 conferma.setVisibility(1);

					    }
					});

				} catch (Exception e) {
					runOnUiThread(new Runnable() {
		                @Override
		                public void run() {
		                		Utils.problemaConnessione(context);
		                }
			    	});
				}

			}
		});
		
		thread.start();

	}
}
