/**
 * Automatically generated file. DO NOT MODIFY
 */
package it.local.seilocal;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "it.local.seilocal";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 7;
  public static final String VERSION_NAME = "1.6";
}
